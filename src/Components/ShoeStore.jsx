import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailsModal from "./DetailsModal";
import ListShoe from "./ListShoe";

export default class ShoeStore extends Component {
  state = {
    data: dataShoe,
    detail: dataShoe[0],
    cart: [],
    cartQty: 0,
    total: 0,
  };
  handleChangeDetails = (shoe) => {
    this.setState({
      detail: shoe,
    });
  };

  handleAdd2Cart = (shoe) => {
    let cartList = [...this.state.cart];
    let index = cartList.findIndex((item) => {
      return item.id === shoe.id;
    });

    if (index === -1) {
      cartList.push({ ...shoe, qty: 1 });
    } else cartList[index].qty++;

    this.setState({
      cart: cartList,
    });

    this.handleCalculateTotalCart(cartList);
  };

  handleCalculateTotalCart = (cartList) => {
    let qty = 0;
    let sum = 0;

    cartList.forEach((item) => {
      qty += item.qty;
      sum += item.qty * item.price;
    });

    this.setState({
      cartQty: qty,
      total: sum,
    });
  };

  handleAddNumber = (index) => {
    let cloneCart = [...this.state.cart];
    cloneCart[index].qty++;
    this.setState({ cart: cloneCart });
    this.handleCalculateTotalCart(cloneCart);
  };

  handleReduceNumber = (index) => {
    let cloneCart = [...this.state.cart];
    cloneCart[index].qty--;
    if (cloneCart[index].qty === 0) {
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
    this.handleCalculateTotalCart(cloneCart);
  };

  handleCheckout = () => {
    this.setState({
      cart: [],
      cartQty: 0,
      total: 0,
    });
    alert("Thank you for shopping");
  };

  handleRemoveCartItem = (index) => {
    let cloneCart = [...this.state.cart];
    cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
    this.handleCalculateTotalCart(cloneCart);
  };

  render() {
    return (
      <div className="container py-5">
        <h2 className="mb-4">Shoe Store</h2>
        <Cart
          cart={this.state.cart}
          total={this.state.total}
          cartQty={this.state.cartQty}
          handleCheckout={this.handleCheckout}
          handleAddNumber={this.handleAddNumber}
          handleReduceNumber={this.handleReduceNumber}
          handleRemoveCartItem={this.handleRemoveCartItem}
        />
        <DetailsModal detail={this.state.detail} />
        <ListShoe
          data={this.state.data}
          handleChangeDetails={this.handleChangeDetails}
          handleAdd2Cart={this.handleAdd2Cart}
        />
      </div>
    );
  }
}
