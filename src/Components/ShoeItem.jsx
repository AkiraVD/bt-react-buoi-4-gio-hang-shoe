import React, { Component } from "react";

export default class ShoeItem extends Component {
  render() {
    let shoe = this.props.shoe;
    return (
      <div className="col-4 mb-4">
        <div className="card h-100 p-3">
          <img className="card-img-top" src={shoe.image} alt="Title" />
          <div className="card-body d-flex flex-column justify-content-between">
            <div>
              <h4 className="card-title">{shoe.name}</h4>
              <h4 className="card-text">${shoe.price}</h4>
            </div>
            <div className="mt-3 d-flex justify-content-center">
              <button
                className="btn btn-warning me-3"
                onClick={() => {
                  this.props.handleAdd2Cart(shoe);
                }}
              >
                Add
              </button>
              <button
                className="btn btn-info"
                data-bs-toggle="modal"
                data-bs-target="#shoeDetails"
                onClick={() => {
                  this.props.handleChangeDetails(shoe);
                }}
              >
                Details
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
