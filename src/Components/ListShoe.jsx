import React, { Component } from "react";
import ShoeItem from "./ShoeItem";

export default class ListShoe extends Component {
  handleRenderListShoe = () => {
    let data = this.props.data;
    return data.map((item) => {
      return (
        <ShoeItem
          shoe={item}
          handleChangeDetails={this.props.handleChangeDetails}
          handleAdd2Cart={this.props.handleAdd2Cart}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.handleRenderListShoe()}</div>;
  }
}
