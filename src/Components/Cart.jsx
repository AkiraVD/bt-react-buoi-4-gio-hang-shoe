import React, { Component } from "react";

export default class Cart extends Component {
  handleRenderCartList = () => {
    return this.props.cart.map((item, index) => {
      let sum = item.qty * item.price;
      return (
        <tr className="align-middle">
          <td>{item.id}</td>
          <td>
            <img style={{ height: "50px" }} src={item.image} alt="" />
          </td>
          <td>{item.name}</td>
          <td>
            <button
              className="btn"
              style={{ color: "gold" }}
              onClick={() => {
                this.props.handleReduceNumber(index);
              }}
            >
              -
            </button>
            {item.qty}
            <button
              className="btn"
              style={{ color: "gold" }}
              onClick={() => {
                this.props.handleAddNumber(index);
              }}
            >
              +
            </button>
          </td>
          <td>${sum}</td>
          <td>
            <button
              onClick={this.props.handleRemoveCartItem}
              className="btn text-danger"
            >
              <i class="fa fa-times"></i>
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        {/* Modal trigger button */}
        <button
          type="button"
          id="cartBtn"
          className="btn"
          data-bs-toggle="modal"
          data-bs-target="#cart"
        >
          <i className="fa fa-shopping-cart position-relative text-warning"></i>
          <span className="bg-danger d-block position-absolute">
            {this.props.cartQty}
          </span>
        </button>
        {/* Modal Body */}
        <div
          className="modal fade"
          id="cart"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modalTitleId"
          aria-hidden="true"
        >
          <div
            className="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-xl"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="modalTitleId">
                  Your Cart
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                />
              </div>
              <div className="modal-body">
                <table className="table">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Image</th>
                      <th>Name</th>
                      <th>Qty</th>
                      <th>Cost</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>{this.handleRenderCartList()}</tbody>
                </table>
              </div>
              <div className="modal-footer justify-content-between">
                <h3 className="ms-5">Total: ${this.props.total}</h3>
                <button
                  type="button"
                  className="btn btn-success"
                  data-bs-dismiss="modal"
                  onClick={() => {
                    this.props.handleCheckout();
                  }}
                >
                  Checkout
                </button>
              </div>
            </div>
          </div>
        </div>
        {/* Optional: Place to the bottom of scripts */}
      </div>
    );
  }
}
